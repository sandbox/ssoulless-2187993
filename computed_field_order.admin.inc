<?php

/**
 * @file
 * Provides the field_collection module admin pages.
 */

/**
 * Menu callback: Settings form.
 */
function computed_field_order_settings_form($form, &$form_state, $entity_type = NULL, $bundle = NULL) 
{
  // Let's get a list of all the entity_type/bundles that have computed fields in them.
  $query = db_select('field_config', 'fc');
  $query->innerJoin('field_config_instance', 'fci', 'fci.field_id = fc.id');
  $query->condition('fc.type', 'computed');
  $query->condition('fc.deleted', 0);
  $query->fields('fci', array('entity_type', 'bundle', 'data'))
    ->orderBy('fci.entity_type')
    ->orderBy('fci.bundle');
  $result = $query->execute();

  $items = array();
  foreach ($result as $row) {
    // Build a multi-level list.
    if (isset($items[$row->entity_type])) {
      $classes = array(
        'computed_field_order',                // Standard class
        drupal_html_class($row->entity_type),  // and entity_type
        drupal_html_class($row->bundle),       // and bundle.
        );

      // It was serendipitous, but using the bundle name as a key prevents duplicates.
      // using Distinct in the query resulted in missed data.
      $items[$row->entity_type]['children'][$row->bundle] = array(
        'data' => l($info['bundles'][$row->bundle]['label'],
          "admin/config/content/computed_field_order/$row->entity_type/" . $row->bundle),
        'class' => $classes,
        );
    }
    else {
      dpm("entro");
      $info = entity_get_info($row->entity_type);
      $items[$row->entity_type] = array(
        'data' => check_plain($info['label']),
        'children' => array(),
        );
    }
  }
  dpm($items);  // Show the list so they can select which one to order.
  $form['intro'] = array(
    '#type' => 'fieldset',
    '#title' => t('Computed Field usage'),
    '#collapsible' => TRUE,
    '#collapsed' => !empty($entity_type),
    '#description' => t('These entity_type / bundle types contain computed fields.'),
    );

  $form['intro']['types'] = array(
    '#type' => 'markup',
    '#markup' => theme('item_list', array('items' => $items)),
    );

  $form['intro']['description'] = array(
    '#type' => 'markup',
    '#markup' => '<div class="description">'
      . t('Click on a bundle name to expose the computed fields for that bundle.')
      . '</div>',
    );

  // If an entity_type is present in the URL, then we show those fields.
  if (!empty($entity_type)) {
    $info = entity_get_info($entity_type);

    $form['#title'] = t("Computed Fields in @type: @bundle.",
      array(
        '@type' => $info['label'],
        '@bundle' => $info['bundles'][$bundle]['label'],
        ));

    $form['#subtitle'] = t('These computed fields are in this entity_type / bundle.');

    // Get the instance data.
    $query = db_select('field_config', 'fc');
    $query->innerJoin('field_config_instance', 'fci', 'fci.field_id = fc.id');
    $query->condition('fc.type', 'computed');
    $query->condition('fci.deleted', 0);
    $query->condition('fci.entity_type', $entity_type);
    $query->condition('fci.bundle', $bundle);
    $query->fields('fci', array('id', 'field_name'));
    $query->orderBy('fci.id');
    $result = $query->execute();

    $form['#entity_type'] = $entity_type;
    $form['#bundle'] = $bundle;

    // Need this to get all the values back.
    $form['fields']['#tree'] = TRUE;
    $ids = array();

    foreach ($result as $row) {
      $instance = field_info_instance($entity_type, $row->field_name, $bundle);
      $title = t('@label (@name)',
        array(
          '@label' => $instance['label'],
          '@name' => $row->field_name,
          ));

      $ids[$row->field_name] = $row->id;

      $form['fields'][$row->field_name] = array(
        'id' => array(
          '#type' => 'markup',
          '#markup' => $row->id,
          ),
        'name' => array(
          '#type' => 'markup',
          '#markup' => $title,
          ),
        'weight' => array(
          '#type' => 'weight',
          '#title' => t('Weight'),
          '#delta' => 100,   // @TODO: This needs to be more than the number of fields.
          '#default_value' => $row->id,
          '#title-display' => 'invisible',
          '#attributes' => array('class' => array('computed-field-order-weight'))
          ),
        );
    }

    $form['#ids'] = $ids;

    $form['note'] = array(
      '#type' => 'markup',
      '#markup' => '<div class="description">'
        . t('Note: Changing the order of field computation in this bundle will not affect
          the order in any other bundles in which the same fields may appear.')
        . '</div>',
      );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array('#type' => 'submit', '#value' => t("Change order"));
  }

  return $form;
}